# P III - Algoritmo Genético

P III (Programación III). Aplicación que determina el equipo de fútbol ideal, mediante el calculo de coeficientes de los jugadores que se dan por sus diferentes variantes utilizando Algoritmos genéticos.
Desarrollo en conjunto con Ignacio Nahuel Rudyk (Estudiante de la UNGS). Ignacio desarrolló la interfaz y la persistencia, mientras que yo desarrollé el algoritmo generador y los casos de prueba.
