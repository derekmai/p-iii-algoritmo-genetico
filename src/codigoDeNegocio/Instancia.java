package codigoDeNegocio;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Instancia implements Serializable
{
	private static final long serialVersionUID = 1L;
	private final int tamanoEquipo = 11;
	private ArrayList<Jugador> jugadores;
	private int jugadoresTotales;
	
	public Instancia()
	{
		this.jugadores = new ArrayList<Jugador>();
		this.jugadoresTotales = 0;
	}
	
	public void agregarJugador(Jugador jugador)
	{
		chequearQueElJugadorNoEsteEnLaInstancia(jugador);
		this.jugadoresTotales++;
		this.jugadores.add(jugador);
	}
	
	private void chequearQueElJugadorNoEsteEnLaInstancia(Jugador jugador)
	{
		Pattern pattern = Pattern.compile(jugador.getNombre(),Pattern.CASE_INSENSITIVE);
		for (Jugador jugadorOtro : jugadores)
		{
			Matcher matcher = pattern.matcher(jugadorOtro.getNombre());
			if (matcher.find())
				throw new IllegalArgumentException("El jugador ya esta en la instancia.");
		}
	}


	public int tamanoEquipo()
	{
		return this.tamanoEquipo;
	}
	
	public int tamanoJugadores()
	{
		return this.jugadoresTotales;
	}

	public Jugador getJugador(int i)
	{
		if (i == -1)
			return null;
		return this.jugadores.get(i);
	}
	
	public Jugador getJugador(String nombre)
	{
		Pattern pattern = Pattern.compile(nombre,Pattern.CASE_INSENSITIVE);
		
		for(Jugador j : this.jugadores)
		{
			Matcher matcher = pattern.matcher(j.getNombre());
			if(matcher.find())
			{
				System.out.println("hubo match");
				return j;
			}
		}
		System.out.println("No hubo match");
		return null;
	}
	
	public double getCoeficienteDelJugador(int i)
	{
		if (i == -1)
			return 0;
		return this.jugadores.get(i).getCoeficienteJugador();
	}
	
	public Pais getNacionalidadDelJugador(int i)
	{
		return jugadores.get(i).getNacionalidad();
	}
	
	public Posicion[] getPosicionesDelJugador(int i)
	{
		return jugadores.get(i).getPosiciones();
	}

	public int getIndexOf(Jugador jugador)
	{
		for (int i = 0; i < jugadoresTotales;i++)
		{
			if (jugadores.get(i).getNombre().equalsIgnoreCase(jugador.getNombre()))
				return i;
		}
		return -1;
	}
}
