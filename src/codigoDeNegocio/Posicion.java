package codigoDeNegocio;

import java.io.Serializable;

public enum Posicion implements Serializable
{
	arquero, 
	lateralIzq, 
	centralIzq,
	centralDer, 
	lateralDer, 
	volanteIzq, 
	volanteCentral, 
	volanteDer, 
	punteroIzq, 
	centroDelantero, 
	punteroDer
}
