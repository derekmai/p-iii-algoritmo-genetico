package codigoDeNegocio;

import java.util.Random;

public class GeneradorAleatorio implements Generador
{

	Random random = new Random();
	
	
	@Override
	public int nextInt(int i)
	{
		return random.nextInt(i);
	}

}
