package codigoDeNegocio;

import java.util.ArrayList;

public class Individuo implements Comparable<Individuo>,Cloneable
{
	private Integer[] bits;
	private Instancia instancia;
	private static Generador generador;
	private int[] jugadoresPorEquipo = new int[32];
	private boolean[] posicionesOcupadas = new boolean[11];
	private Integer[] numeroJugadorEnPosicion = new Integer[11];
	private int cantidadTarjetas = 0;
	private int jugadoresSinGoles = 0;
	private double fitness;
	
	public static void setGenerador(Generador Random)
	{
		generador = Random;
	}
	
	public Individuo(Instancia instancia)
	{
		this.instancia = instancia;
		this.bits = new Integer[this.instancia.tamanoEquipo()];			
		for (int i = 0 ; i < bits.length ; i ++)
		{
			int next = generador.nextInt(instancia.tamanoJugadores());
			while (isJugadorRepetido(next,i))
			{
				next = generador.nextInt(instancia.tamanoJugadores());
			}
			bits[i] = next;
		}
	}
	
	Individuo(Instancia instancia, Integer[] bits)
	{
		this.bits = bits;
		this.instancia = instancia;
	}

	public Individuo()
	{
		bits = new Integer[11];
		for (int i = 0; i < bits.length; i++)
			bits[i] = -1;
	}

	public void mutar()
	{
		int posicionAMutar = generador.nextInt(bits.length);
		int jugadorAPoner = generador.nextInt(instancia.tamanoJugadores());
		bits[posicionAMutar] = jugadorAPoner;
	}
	
	public Individuo[] recombinar(Individuo otro)
	{
		int puntoDeRecombinacion = generador.nextInt(instancia.tamanoEquipo());
		
		Integer[] bits1 = new Integer[instancia.tamanoEquipo()];
		Integer[] bits2 = new Integer[instancia.tamanoEquipo()];
		
		for (int i = 0; i < puntoDeRecombinacion ; i++)
		{
			bits1[i] = this.get(i);
			bits2[i] = otro.get(i);
		}
		
		for (int i = puntoDeRecombinacion ; i < instancia.tamanoEquipo();i++)
		{
			bits1[i] = otro.get(i);
			bits2[i] = this.get(i);
		}
		
		Individuo hijo1 = new Individuo(instancia, bits1);
		Individuo hijo2 = new Individuo(instancia, bits2);
		if (hijo1.tieneJugadoresRepetidos())
			hijo1 = new Individuo(instancia);
		if (hijo2.tieneJugadoresRepetidos())
			hijo2 = new Individuo(instancia);
		return new Individuo[] { hijo1, hijo2 };
				
	}
	private boolean tieneJugadoresRepetidos()
	{
		for (int i = 0; i < bits.length;i++)
			for (int j = i+1;j<bits.length;j++)
				if (bits[i] == bits[j])
					return true;
		return false;
	}

	/*public Individuo[] recombinar(Individuo otro)
	{
		int puntoDeRecombinacion = generador.nextInt(instancia.tamanoEquipo());
		
		Integer[] bits1 = new Integer[instancia.tamanoEquipo()];
		Integer[] bits2 = new Integer[instancia.tamanoEquipo()];
		
		for (int i = 0; i < puntoDeRecombinacion ; i++)
		{
			bits1[i] = this.get(i);
			bits2[i] = otro.get(i);
		}
		
		for (int i = puntoDeRecombinacion ; i < instancia.tamanoEquipo();i++)
		{
			bits1[i] = otro.get(i);
			bits2[i] = this.get(i);
		}
		
		Individuo hijo1 = new Individuo(instancia, bits1);
		Individuo hijo2 = new Individuo(instancia, bits2);
		
		return new Individuo[] { hijo1, hijo2 };
				
	}*/
	
	private Integer get(int i)
	{
		return this.bits[i];
	}
	
	public double fitness()
	{
		reiniciarEstadisticasDelEquipo();
		int ret = 0;
		for(int i = 0; i < bits.length;i++)
		{
			if (bits[i] != -1)
			{
				if (pasaCriteriosDePenalizacion(i))
				{
					ret += getCoeficienteDelJugador(bits[i]);
				}
				else
					ret = penalizar(ret,i);//arbitrario
				jugadoresPorEquipo[posicionDelPaisEnElArreglo(bits[i])]++;
				revisarSiTieneTarjetas(i);
				revisarSiNoHizoGoles(i);
			}
			else
				ret += 0;
		}
		fitness = ret;
		return ret;
	}

	private void reiniciarEstadisticasDelEquipo()
	{
		cantidadTarjetas = 0;
		jugadoresSinGoles = 0;
		posicionesOcupadas = new boolean[11];
		jugadoresPorEquipo = new int[32];
		numeroJugadorEnPosicion = new Integer[11];
	}

	private int penalizar(int ret,int i)
	{
		if (isEquipoExcedidoDeJugadoresDelMismoPais(bits[i]))
			ret = ret - 200;
		if (damePosicionLibreDelJugador(bits[i]) != -1)
			ret = ret - 150;
		if (isJugadorRepetido(bits[i],i))
			ret = ret - 1500;
		if (isEquipoExcedidoDeTarjetas())
			ret = ret -50;
		return ret;
		
	}

	private boolean pasaCriteriosDePenalizacion(int i)
	{
		return  !isEquipoExcedidoDeJugadoresDelMismoPais(bits[i])
				&& damePosicionLibreDelJugador(bits[i]) != -1
				&& !isJugadorRepetido(bits[i],i)
				&& !isEquipoExcedidoDeTarjetas()
				&& !isEquipoExcedidoDeJugadoresSinGoles();
	}

	private boolean isEquipoExcedidoDeJugadoresSinGoles()
	{
		return jugadoresSinGoles >= 6;
	}

	boolean isEquipoExcedidoDeJugadoresDelMismoPais(int i)
	{
		return jugadoresPorEquipo[posicionDelPaisEnElArreglo(i)] >= 4;
	}
	
	int posicionDelPaisEnElArreglo(int i)
	{
		int ret =  Pais.valueOf(instancia.getNacionalidadDelJugador(i).toString()).ordinal();
		return ret == 32? 31:ret;
	}
	
	public int damePosicionLibreDelJugador(Integer jugadorI)
	{
		int ret = -1;
		int[] indexesPosicionesJugador = indexDeLaPosicionEnElArreglo(jugadorI);
		for (int j = 0 ; j < indexesPosicionesJugador.length ; j++)
		{
			boolean aux = posicionesOcupadas[indexesPosicionesJugador[j]];
			if (!aux)
			{
				numeroJugadorEnPosicion[indexesPosicionesJugador[j]] = jugadorI;
				posicionesOcupadas[indexesPosicionesJugador[j]] = true;
				return indexesPosicionesJugador[j];
			}
		}
		return ret;
	}

	int[] indexDeLaPosicionEnElArreglo(int i)
	{
		Posicion[] posiciones = instancia.getJugador(i).getPosiciones();
		int[] ret = new int[posiciones.length];
		for (int j = 0; j < posiciones.length;j++)
			ret[j] = Posicion.valueOf(posiciones[j].toString()).ordinal();
		return ret;
	}
	
	boolean isJugadorRepetido(int jugador, int j)
	{
		for(int i = 0 ; i < bits.length ; i++)
		{
			if (bits[i]!=null &&i !=j)
				if (jugador == bits[i])
					return true;
		}
		return false;
	}
	
	private boolean isEquipoExcedidoDeTarjetas()
	{
		return cantidadTarjetas >= 5;
	}

	double getCoeficienteDelJugador(int i)
	{
		return instancia.getCoeficienteDelJugador(i);
	}

	private void revisarSiNoHizoGoles(int i)
	{
		if (!tieneGoles(bits[i]))
			jugadoresSinGoles++;
	}
	
	private void revisarSiTieneTarjetas(int i)
	{
		if (tieneTarjetas(bits[i]))
			cantidadTarjetas++;
	}
	
	private boolean tieneTarjetas(Integer i)
	{
		return instancia.getJugador(i).getCantidadTarjetas() > 0;
	}

	private boolean tieneGoles(Integer i)
	{
		return instancia.getJugador(i).getCantidadDeGoles() > 0;
	}
	

	public Jugador getJugadorEnPosicion(int i)
	{
		return instancia.getJugador(bits[i]);
	}

	public int tamano()
	{
		return bits.length;
	}

	public void setJugadorEnPosicion(Jugador jugador, int j)
	{
		int jugadorEnInt = instancia.getIndexOf(jugador);
		bits[j] = jugadorEnInt;
	}

	public void setInstancia(Instancia instancia2)
	{
		this.instancia = instancia2;
	}

	@Override
	public int compareTo(Individuo o)
	{
		if(this.fitness()<o.fitness())
		{
			return -1;
		}
		else if(this.fitness()> o.fitness())
		{
			return 1;
		}
		return 0;
	}

	public boolean isposicionOcupada(int index)
	{
		return posicionesOcupadas[index];
	}
	
	@Override
	public Object clone() throws CloneNotSupportedException
	{
		return super.clone();
	}
	
	public void ordenarPorPosicion()
	{
		reiniciarEstadisticasDelEquipo();
		ArrayList<Integer>jugadoresSinPosicion = new ArrayList<Integer>();
		for (int i = 0; i < 11; i++)
		{
			//Jugador jugador = instancia.getJugador(bits[i]);
			int posicionLibre = damePosicionLibreDelJugador(bits[i]);
			if (posicionLibre !=-1)
				numeroJugadorEnPosicion[posicionLibre] = bits[i];
			else
			{
				jugadoresSinPosicion.add(bits[i]);
			}
		}
		for(Integer jugador : jugadoresSinPosicion)
			for(int i = 0; i<11;i++)
				if (numeroJugadorEnPosicion[i]==null)
					numeroJugadorEnPosicion[i] = jugador;
		bits = numeroJugadorEnPosicion;
	}

	public double getFitness()
	{
		return this.fitness;
	}

}

