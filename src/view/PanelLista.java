package view;

import java.awt.Color;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;

import javax.swing.JPanel;

import codigoDeNegocio.Individuo;
import codigoDeNegocio.Jugador;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;

public class PanelLista extends JPanel implements ItemListener
{
	private static final long serialVersionUID = 1L;
	private ArrayList<Jugador> listaJugadores;
	private VentanaEquipo ventanaPadre;
	private JComboBox<String> listaNombres;
	private JButton btnAgregarEnCancha;
	private PanelEstadisticas panelEstadisticas;
	
	public PanelLista(VentanaEquipo ventanaPadre)
	{
		this.ventanaPadre = ventanaPadre;
		configurarPanel(ventanaPadre.getBounds());
		configurarComponentes();
	}
	
	private void configurarPanel(Rectangle dimensionVentana) 
	{
		this.setBounds((dimensionVentana.width/4)*3, 0, dimensionVentana.width/4, dimensionVentana.height-30);
		this.setBackground(new Color(86, 7, 12));
		this.setVisible(true);
		this.setLayout(null);
		this.panelEstadisticas = new PanelEstadisticas(0,30,this.getWidth()-5,this.getHeight()-90,12);
		this.add(panelEstadisticas);
	}
	
	private void configurarComponentes() 
	{
		//this.preseleccionados = new String[]{"Messi", "Higuain", "Dybala", "Aguero", "Di Maria", "Lanzini", "Pavon", "Biglia", "Mascherano", "Ansaldi", "Meza", "Mercado", "Otamendi", "Rojo", "Tagliafico", "Armani", "Caballero", "Guzman"};
		//LISTA DE JUGADORES PARA LUEGO MOSTRAR SUS DATOS EN PANTALLA
		this.listaJugadores = new ArrayList<Jugador>();
		
		//LISTA DE NOMBRES EN PANTALLA
		this.listaNombres = new JComboBox<String>();
		this.listaNombres.addItem("Mi equipo");
		this.listaNombres.setBounds(15,5,170,25);
		Font fuente = new Font(null, Font.CENTER_BASELINE,20);
		this.listaNombres.setFont(fuente);
		listaNombres.addItemListener(this);
		this.add(listaNombres);
		
		//BOTON AGREGAR A EQUIPO
		this.btnAgregarEnCancha = new JButton("Agregar al equipo");
		this.btnAgregarEnCancha.setBounds(10, this.getBounds().height-50,this.getBounds().width-20,30);
		this.btnAgregarEnCancha.addMouseListener(this.ventanaPadre);
		this.btnAgregarEnCancha.addActionListener(this.ventanaPadre);
		this.add(this.btnAgregarEnCancha);
	}

	public String getJugadorSelected() 
	{
		return this.listaNombres.getItemAt(this.listaNombres.getSelectedIndex());
	}

	public void agregarJugador(Jugador jugador) 
	{
		if(jugador == null)
		{
			JOptionPane.showMessageDialog(this,"No se encontro el jugador.");
		}
		else
		{
			for(Jugador j : listaJugadores)
				if(j.getNombre().equalsIgnoreCase(jugador.getNombre()))
				{
						throw new IllegalArgumentException("el jugador ya esta en la lista.");
						
				}
			this.listaNombres.addItem(jugador.getNombre());
			this.listaJugadores.add(jugador);
		}
	}

	private Jugador dameJugadorSeleccionado(String item) 
	{
		if(item.equals("Mi equipo"))
		{
			return null;
		}
		for(Jugador j : this.listaJugadores)
		{
			if(j.getNombre().equalsIgnoreCase(item))
			{
				return j;
			}
		}
		return null;
	}

	@Override
	public void itemStateChanged(ItemEvent e)
	{
		Jugador jugadorSeleccionado = this.dameJugadorSeleccionado(this.listaNombres.getItemAt(this.listaNombres.getSelectedIndex()));
		if (jugadorSeleccionado == null)
		{
			System.out.println("null");
			panelEstadisticas.setVisible(false);
		}
		else
		{
			this.panelEstadisticas.mostrarDatosJugador(jugadorSeleccionado);
			panelEstadisticas.setVisible(true);
			this.panelEstadisticas.paintComponent(getGraphics());
			this.ventanaPadre.setJugadorSeleccionado(jugadorSeleccionado);
		}
		
	}

	public void setLista(Individuo individuo)
	{
		this.listaJugadores = new ArrayList<Jugador>();
		listaNombres.removeAll();
		for (int i = 0 ; i<11;i++)
		{
			Jugador jugador = individuo.getJugadorEnPosicion(i);
			agregarJugador(jugador);
		}
	}     
}
