package view;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import codigoDeNegocio.Individuo;
import codigoDeNegocio.Jugador;
import codigoDeNegocio.Posicion;

public class PanelCancha extends JPanel implements MouseListener
{
	private static final long serialVersionUID = 1L;
	private ArrayList<Rectangle> posiciones;
	private Individuo equipo;
	private VentanaEquipo padre;
	private boolean[] posicionOcupada = new boolean [11];

	public PanelCancha(VentanaEquipo ventanaPadre)
	{
		
		equipo = new Individuo();
		this.padre = ventanaPadre;
		equipo.setInstancia(padre.getPadre().getControlador().getInstancia());
		this.configurarPanel();
		this.definirPosiciones();
	}

	private void configurarPanel()
	{
		this.setBounds(0,0,510,330);
		this.setLayout(null);
		this.setBackground(new Color(86, 7, 12));
		this.setVisible(true);
		this.addMouseListener(this);
	}
	
	private void definirPosiciones() 
	{
		this.posiciones = new ArrayList<Rectangle>(11);
		this.posiciones.add(new Rectangle(15,145,20,20));//ARQUERO
		
		this.posiciones.add(new Rectangle(130,20,20,20));//LATERAL IZQUIERDO
		this.posiciones.add(new Rectangle(100,100,20,20));//CENTRAL IZQUIERDO
		this.posiciones.add(new Rectangle(100,180,20,20));//CENTRAL DERECHO
		this.posiciones.add(new Rectangle(130,260,20,20));//LATERAL DERECHO
		
		this.posiciones.add(new Rectangle(260,70,20,20));//VOLANTE IZQUIERDO
		this.posiciones.add(new Rectangle(220,145,20,20));//VOLANTE CENTRAL
		this.posiciones.add(new Rectangle(260,220,20,20));//CENTRAL DERECHO
		
		this.posiciones.add(new Rectangle(380,70,20,20));//PUNTERO IZQUIERDO
		this.posiciones.add(new Rectangle(340,145,20,20));//PUNTERO CENTRAL
		this.posiciones.add(new Rectangle(380,220,20,20));//CENTRAL DERECHO
	}

	public boolean seHizoClicEnCancha(MouseEvent e) 
	{
		for(Rectangle zona :posiciones)
		{
			if(zona.contains(e.getPoint()))
			{
				return true;
			}
		}
		return false;
	}

	public void setJugador(Jugador jugador) 
	{
		if(noEstaEnCancha(jugador))
		{
			Posicion[] posiciones = jugador.getPosiciones();
			for (int i = 0; i < posiciones.length;i++)
			{
				int index = Posicion.valueOf(posiciones[i].toString()).ordinal();
				if (!posicionOcupada[index])
				{
					equipo.setJugadorEnPosicion(jugador, index);
					posicionOcupada[index] = true;
					return;
				}
			}
			String mensaje = (posiciones.length > 1 ? "ya existen jugadores en las posiciones ":" ya existe un jugador en la posicion ");
			String mensaje2 = (posiciones.length > 1 ? "Desea reemplazar alguno?":"Desea reemplazarlo?");
			Object[]botones = {"Si","No"};
			int respuesta = JOptionPane.showOptionDialog(this.padre,mensaje + "del jugador "+jugador.getNombre()+".\n"+mensaje2,null, JOptionPane.YES_NO_CANCEL_OPTION,JOptionPane.PLAIN_MESSAGE,null, botones,null);
			if (respuesta == JOptionPane.YES_OPTION)
			{
					int index = Posicion.valueOf(posiciones[0].toString()).ordinal();
					equipo.setJugadorEnPosicion(jugador, index);
					posicionOcupada[index] = true;
					return;
			}
		}
	}
	
	private boolean noEstaEnCancha(Jugador jugador)
	{
		for(int i = 0 ; i < 11 ; i++)
		{
			Jugador j = this.equipo.getJugadorEnPosicion(i);
			if(j != null)
			{
				if(j.getNombre().equals(jugador.getNombre()))
				{
					return false;
				}
			}
		}
		return true;
	}

	@Override
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		g.drawImage(cargarImagen("src/view/cancha.jpg"), 0, 0, this.getWidth()-5, this.getHeight(), null,null);
		Image cara = null;
		for(int i = 0 ; i < 11 ; i++)
		{
			Jugador jugador = equipo.getJugadorEnPosicion(i);
			if(jugador != null)
			{
				cara = this.cargarImagen("src/view/jugadores/"+jugador.getNombre()+".png");
				g.drawImage(cara, this.posiciones.get(i).x, this.posiciones.get(i).y, this.posiciones.get(i).width+60, this.posiciones.get(i).height+40, null, null);
			}
			else
			{
				g.drawRect(this.posiciones.get(i).x, this.posiciones.get(i).y, this.posiciones.get(i).width, this.posiciones.get(i).height);
			}
		}
	}
	
	
	public Image cargarImagen(String ruta)
	{
		File archivo = new File(ruta);
		
		try 
		{
			if(archivo.exists())
			{
				return ImageIO.read(archivo);
			}
			else
			{
				archivo = new File("src/view/jugadores/null.png");
				return ImageIO.read(archivo);
			}		
		} 
		catch (IOException e) 
		{
			System.out.println("hubo un error al cargar la imagen");
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void mouseClicked(MouseEvent e)
	{
		System.out.println("Se clickeo en la cancha");
		if(seHizoClicEnCancha(e))
		{
			System.out.println("click en cuadrado de jugador");
			Jugador jugador = padre.getJugadorSeleccionado();
			setJugador(jugador);
			this.paintComponent(getGraphics());
		}
	}

	@Override
	public void mousePressed(MouseEvent e)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e)
	{
		// TODO Auto-generated method stub
		
	}

	public Individuo getEquipo()
	{
		return this.equipo;
	}

	public void setEquipo(Individuo equipo2)
	{
		this.equipo = equipo2;
	}
}
