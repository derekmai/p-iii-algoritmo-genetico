package view;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import codigoDeNegocio.Jugador;
import codigoDeNegocio.Posicion;

public class PanelEstadisticas extends JPanel
{
	private static final long serialVersionUID = 1L;
	private String nombreDeFoto;
	private JTextArea fichero;
	
	public PanelEstadisticas(int x, int y, int width, int height,int tamanoLetra)
	{
		configurarPanel(x, y, width, height,tamanoLetra);
	}

	private void configurarPanel(int x, int y, int width, int height,int tamanoLetra) 
	{
		this.setLayout(null);
		this.setBounds(x, y, width, height);
		this.setVisible(true);
		this.setBackground(null);
		
		this.fichero = new JTextArea();
		fichero.setEditable(false);
		fichero.setLocation(10, 10);
		fichero.setSize(this.getSize().width,this.getSize().height/2-40);
		fichero.setBackground(null);
		Font f = new Font(null,Font.LAYOUT_LEFT_TO_RIGHT,tamanoLetra);
		fichero.setForeground(Color.BLACK);
		fichero.setFont(f);
		this.add(fichero);
	}
	
	public void mostrarDatosJugador(Jugador jug) 
	{
		if(jug != null)
		{
			Font f = new Font(null, Font.CENTER_BASELINE,12);
			fichero.setFont(f);
			fichero.setForeground(new Color(212, 175, 55));
			fichero.setText("Nombre: "+ jug.getNombre()+"\nNacionalidad: "+jug.getNacionalidad()+"\nPosicion: "+this.damePosicionesString(jug.getPosiciones())+"\nTarjetas: "+jug.getCantidadTarjetas()+"\nFaltas: "+jug.getFaltas()+"\nGoles: "+jug.getCantidadDeGoles()+"\nPuntaje Promedio: "+jug.getPuntajePromedio());
			this.nombreDeFoto = jug.getNombre();
			this.repaint();
		}
	}

	public void cambiarFuente(Font f)
	{
		fichero.setFont(f);
	}
	
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		if(this.nombreDeFoto != null)
		{
			Image foto = this.dameImagen("src/view/jugadores/"+this.nombreDeFoto+".png");
			if(foto != null)
			{
				g.drawImage(foto, (this.getWidth()/4)/2, this.getHeight()/2-10, this.getWidth()-(this.getWidth()/4), this.getHeight()/2+30,null,null);
			}
		}
	}
	
	private Image dameImagen(String ruta)
	{
		File archivo = new File(ruta);
		try 
		{
			if(archivo.exists())
			{
				return ImageIO.read(archivo);
			}
			else
			{
				archivo = new File("src/view/jugadores/"+this.nombreDeFoto+".jpg");
				if(archivo.exists())
				{
					return ImageIO.read(archivo);
				}
				else
				{
					archivo = new File("src/view/jugadores/null.png");
					return ImageIO.read(archivo);
				}
			}	
		} 
		catch (IOException e) 
		{
			System.out.println("hubo un error al cargar la imagen");
			e.printStackTrace();
		}
		return null;
	}

	private String damePosicionesString(Posicion[] posiciones)
	{
		String ret = "";
		for (int i = 0; i < posiciones.length;i++)
		{
			if (i == posiciones.length-1)
				ret += posiciones[i].toString();
			else
			{
				ret += posiciones[i].toString() + "\n";
			}
		}
		return "["+ret+"]";
	}
}
