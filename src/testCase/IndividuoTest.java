package testCase;

import static org.junit.Assert.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.junit.Before;
import org.junit.Test;

import codigoDeNegocio.*;

public class IndividuoTest
{
	private Instancia instancia;

	@Before
	public void inicializar()
	{
		instancia = new Instancia();
		instancia.agregarJugador(new Jugador("Messi", 0, 0, 9.5,new Posicion[]{Posicion.centroDelantero,Posicion.punteroIzq}, Pais.Argentina, 5));
		instancia.agregarJugador(new Jugador("Armani", 0, 0, 8.33,new Posicion[] {Posicion.arquero}, Pais.Argentina, 0));
		instancia.agregarJugador(new Jugador("Otamendi", 6, 2, 7.69,new Posicion[]{Posicion.centralDer,Posicion.centralIzq}, Pais.Argentina, 1));
		instancia.agregarJugador(new Jugador("Dybala", 1, 0, 8.50,new Posicion[]{Posicion.punteroIzq}, Pais.Argentina, 3));
		instancia.agregarJugador(new Jugador("Werner", 0, 0, 9,new Posicion[]{Posicion.punteroDer,Posicion.punteroIzq}, Pais.Alemania, 4));
		instancia.agregarJugador(new Jugador("Hector", 3, 1, 7.83,new Posicion[]{Posicion.lateralIzq}, Pais.Alemania, 0));
		instancia.agregarJugador(new Jugador("Walker", 2, 0, 7,new Posicion[]{Posicion.lateralDer}, Pais.Inglaterra, 1));
		instancia.agregarJugador(new Jugador("Henderson", 0, 0, 7.43,new Posicion[]{Posicion.volanteDer}, Pais.Inglaterra, 2));
		instancia.agregarJugador(new Jugador("Dier", 0, 0, 8.99,new Posicion[]{Posicion.volanteCentral}, Pais.Inglaterra, 1));
		instancia.agregarJugador(new Jugador("Heung-min", 0, 0, 9,new Posicion[]{Posicion.volanteIzq}, Pais.CoreaDelSur, 2));
		instancia.agregarJugador(new Jugador("Young-gwon", 10, 3, 9.5,new Posicion[]{Posicion.centralIzq,Posicion.centralDer}, Pais.CoreaDelSur, 0));		
		//(nombre, faltas, tarjetas, double puntajePromedio, Posicion[] posiciones, Pais nacionalidad, goles) 
	}
	
	@Test
	public void constructorTest()
	{
		Individuo.setGenerador(new GeneradorTesting(new int[]{5,7,3,1,0,2,9,4,8,6,10},0));
		Individuo individuo = new Individuo(instancia);
		
		assertIguales(new String[]{"Hector","Henderson","Dybala","Armani","Messi","Otamendi","Heung-min","Werner","Dier","Walker","Young-gwon"},individuo);
	}

	@Test
	public void construirTest()
	{
		testear("C(5 7 3 1 0 2 9 4 8 6 10) = (5 7 3 1 0 2 9 4 8 6 10)");
		testear("C(10 6 3 1 4 2 8 7 9 0 5) = (10 6 3 1 4 2 8 7 9 0 5)");
	}
	
	@Test
	public void mutarTest()
	{
		testear("M(5 7 3 1 0 2 9 4 8 6 10)(4) = (5 7 3 1 5 2 9 4 8 6 10)");
	}
	
	@Test
	public void recombinarTest()
	{
		testear ("R(5 7 3 1 0 2 9 4 8 6 10 ; 10 6 3 1 4 2 8 7 9 0 5)(0) = (10 6 3 1 4 2 8 7 9 0 5 ; 5 7 3 1 0 2 9 4 8 6 10)");
	}
	
	private void testear(String string)
	{
		if (string.startsWith("C"))
			testearConstruccion(string);
		if (string.startsWith("M"))
			testearMutacion (string);
		if (string.startsWith("R"))
			testearRecombinacion(string);
		
	}
	
	private void testearConstruccion(String string)
	{
		Pattern pattern = Pattern.compile("C\\(([\\d\\s]+)\\) = \\(([\\d\\s]+)\\)");
		Matcher matcher = pattern.matcher(string);
		
		assertTrue(matcher.matches());
		
		Individuo individuo = construir( matcher.group(1) );
		assertIguales(matcher.group(2), individuo);
	}

	private void testearMutacion(String string)
	{
		Pattern pattern = Pattern.compile("M\\(([\\d\\s]+)\\)\\((\\d|10)\\) = \\(([\\d\\s]+)\\)");
		Matcher matcher = pattern.matcher(string);
		
		assertTrue(matcher.matches());
		
		Individuo individuo = construir( matcher.group(1) );
		mutar(individuo, Integer.parseInt(matcher.group(2)));
		assertIguales(matcher.group(3), individuo);
	}

	private void testearRecombinacion(String string)
	{
		Pattern pattern = Pattern.compile("R\\(([\\d\\s]+) ; ([\\d\\s]+)\\)\\((\\d|10)\\) = \\(([\\d\\s]+) ; ([\\d\\s]+)\\)");
		Matcher matcher = pattern.matcher(string);
		
		assertTrue(matcher.matches());
		
		Individuo padre1 = construir( matcher.group(1) );
		Individuo padre2 = construir( matcher.group(2) );
		
		Individuo[] hijos = recombinar(padre1, padre2, Integer.parseInt(matcher.group(3)));
	
		assertIguales(matcher.group(4), hijos[0]);
		assertIguales(matcher.group(5), hijos[1]);
	}
	
	private Individuo construir(String group)
	{
		int[] valores = new int[11];
		int i = 0;
		int inicio = 0;
		int fin = 0;
		while (i<10)
		{
			if (group.charAt(fin) == ' ')
			{
				valores[i] = Integer.parseInt(group.substring(inicio,fin));
				inicio = fin+1;
				i++;
			}
			fin++;
		}
		Individuo.setGenerador(new GeneradorTesting(valores,0));
		return new Individuo(instancia);
	}
	
	private void mutar(Individuo individuo, int numero)
	{
		Generador generador = new GeneradorTesting(new int[] {numero,5}, numero);
		Individuo.setGenerador(generador);
		individuo.mutar();
	}

	private Individuo[] recombinar(Individuo padre1, Individuo padre2,int k)
	{
		Generador generador = new GeneradorTesting(null, k);
		Individuo.setGenerador(generador);
		
		return padre1.recombinar(padre2);
	}

	private void assertIguales(String[] string, Individuo individuo)
	{
		for(int i = 0; i < 11;i++)
			assertEquals(string[i],individuo.getJugadorEnPosicion(i).getNombre());
	}
	
	private void assertIguales(String group, Individuo individuo)
	{
		Individuo otro = construir(group);
		for (int i = 0; i < 11;i++)
			assertEquals(individuo.getJugadorEnPosicion(i).getNombre(),otro.getJugadorEnPosicion(i).getNombre());
	}
}
